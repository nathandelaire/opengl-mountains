//
// Created by natha on 11/07/2021.
//

#include "Engine.hpp"

//TODO : URGENT : TELL TO README QUE LE EXE DOIT ETRE LANCE DANS UN SOUS DOSSIER

int main(){

    Engine engine(1920, 1080, "Mountains");

    while (!glfwWindowShouldClose(engine.getWindow()))
    {
        engine.update();
        engine.render();
    }

    return 0;
}