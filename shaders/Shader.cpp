//
// Created by Pierre on 12/07/2021.
//

#include "Shader.hpp"

Shader::Shader(const char *vFile, const char *gFile, const char *fFile) {

    //GLuint = 0 : non existing shader
    GLuint vertexShader = 0;
    GLuint geometryShader = 0;
    GLuint fragmentShader = 0;

    //Load all shader from files
    vertexShader = loadShader(GL_VERTEX_SHADER, vFile);
    if(gFile)
        geometryShader = loadShader(GL_GEOMETRY_SHADER, gFile);
    fragmentShader = loadShader(GL_FRAGMENT_SHADER, fFile);

    //Link our id program to all the shaders created
    linkProgram(vertexShader,
                geometryShader,
                fragmentShader);

    //End
    glDeleteShader(vertexShader);
    glDeleteShader(fragmentShader);
    if(geometryShader)
        glDeleteShader(geometryShader);
}

Shader::~Shader() {  glDeleteProgram(this->id); }

std::string Shader::loadShaderSource(const char *filename) {
    std::string temp;
    std::string src;

    std::ifstream in_file;

    //Load vertex shader program -> Put text file we did in buffer and compile it
    in_file.open(filename);
    if(in_file.is_open())
    {
        while(std::getline(in_file, temp))
            src += temp + "\n";
    }
    else {
        std::cout << "ERROR::SHADER::FILE_OPENING_ISSUE : " << filename << std::endl;
    }
    in_file.close();
    return src;
}

GLuint Shader::loadShader(GLenum type, const char *filename) {

    char infoLog[512];
    GLint success;

    //Vertex Shader ID
    GLuint shader = glCreateShader(type);

    //Set a source for the shader -> our text file
    std::string str_src = loadShaderSource(filename);
    const GLchar *src = str_src.c_str();
    glShaderSource(shader, 1, &src, NULL);
    //Compile our program
    glCompileShader(shader);
    //Check if compilation success
    glGetShaderiv(shader, GL_COMPILE_STATUS, &success);
    if(!success) {
        glGetShaderInfoLog(shader, 512, NULL, infoLog);
        std::cout << "ERROR::SHADER::ISSUE_ON_SHADER_COMPILE : " << filename << std::endl;
        std::cout << infoLog << std::endl;
    }
    return shader;
}

void Shader::linkProgram(GLuint vertexShader, GLuint geometryShader, GLuint fragmentShader) {

    char infoLog[512];
    GLint success;

    //Create a new program in OpenGL memory and use it
    id = glCreateProgram();
    glUseProgram(id);

    //Link our new program the vertex and fragment (and maybe geometry) program we compiled earlier
    glAttachShader(id, vertexShader);
    //GeometryShader is not mandatory (above 0 if exists)
    if(geometryShader)
        glAttachShader(id, geometryShader);
    glAttachShader(id, fragmentShader);

    glLinkProgram(id);

    glGetProgramiv(id, GL_LINK_STATUS, &success);
    if(!success)
    {
        glGetProgramInfoLog(id, 512, NULL, infoLog);
        std::cout << "ERROR::SHADER::LINK_PROGRAM :" << id << std::endl;
        std::cout << infoLog << std::endl;
    }

    glUseProgram(0);
}

void Shader::use() const { glUseProgram(id); }

void Shader::unuse() const { glUseProgram(0); }
