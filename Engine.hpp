//
// Created by natha on 11/07/2021.
//

#ifndef OPENGL_MOUNTAINS_ENGINE_HPP
#define OPENGL_MOUNTAINS_ENGINE_HPP

#include "shaders/Shader.hpp"
#include "model/HeightMap.hpp"
#include "texture/Texture.hpp"
#include "model/Light.hpp"
#include "model/Model.hpp"
#include "camera/Camera.hpp"
#include "model/skybox/Skybox.hpp"
#include "model/sun/Sun.hpp"

class Engine {

public:

    Engine(int wWidth, int wHeight, const char *title);
    virtual ~Engine();

    void update();
    void render();

    //init variables
    void initGlfw();
    static void framebuffer_resize_callback(GLFWwindow *window, int fbW, int fbH);
    void initWindow();
    void initImGUI();
    void initGlew();
    void initOpenGLOptions();

    void initHeightMap();
    void initShaders();
    void initTextures();
    void initLights();
    void initModel();

    void initCamera();
    void initMatrices();
    void initUniforms();

    void initShadowMap();
    void initTextureBindings();
    void initSkyBox();
    void initSun();

    //Update variables
    void updateDeltaTime();
    void updateMouseInput();
    void updateKeyboardInput();
    void updateImGui();
    void updateUniforms();

    GLFWwindow *getWindow() const;


private:

    //Window
    GLFWwindow *window;
    const int W_WIDTH;
    const int W_HEIGHT;
    int fbwidth;
    int fbheight;
    const char *title;

    //Delta time
    float dt;
    float curTime;
    float lastTime;

    //Mouse variables
    double lastMouseX;
    double lastMouseY;
    double mouseX;
    double mouseY;
    double mouseOffsetX;
    double mouseOffsetY;
    //For first call
    bool firstMouse;
    bool is_Pause = false;

    //Shader
    Shader *shader;

    //Textures
    Texture *heightmap;
    Texture *shadowmap;
    Texture *groud_grass;
    Texture *ground_rock1;
    Texture *ground_rock2;
    Texture *ground_snow;
    Texture *mountain_logo;

    //Main model
    Model *ground;

    //Camera and view matrices
    Camera camera;
    glm::mat4 ViewMatrix;
    glm::mat4 ModelMatrix;
    glm::mat4 ProjectionMatrix;

    //Height map properties
    utils::Image heightMapImage;
    float HM_width;
    float HM_height;
    float HM_vertWidth;
    float HM_vertHeight;
    float HM_amplitude;
    float HM_frequency;
    float HM_power;

    //Skybox
    Skybox *skybox;

    //Sun
    Light *sunlight;
    Sun *sun;
    float sun_x_orientation;
};


#endif //OPENGL_MOUNTAINS_ENGINE_HPP
