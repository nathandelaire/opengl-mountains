//
// Created by Pierre on 14/07/2021.
//

#include "Skybox.hpp"

#include <utility>

Skybox::Skybox(glm::mat4 projection, glm::mat4 view) : ProjectionMatrix(projection), ViewMatrix(view) {
    initSkyBoxtexture();
    initSkyBoxModel();
    initShader();
    initUniforms();
}

void Skybox::initSkyBoxtexture() {
    glActiveTexture(GL_TEXTURE6);
    glGenTextures(1, &skybox_texture_id);
    glBindTexture(GL_TEXTURE_CUBE_MAP, skybox_texture_id);

    std::vector<std::string> texture_face = {"../img/skybox_img/right.bmp",
                                             "../img/skybox_img/left.bmp",
                                             "../img/skybox_img/top.bmp",
                                             "../img/skybox_img/bottom.bmp",
                                             "../img/skybox_img/front.bmp",
                                             "../img/skybox_img/back.bmp"};

    int width, height, nrChannels;
    unsigned char *data;
    for(unsigned int i = 0; i < texture_face.size(); i++){
        data = stbi_load(texture_face[i].c_str(), &width, &height, &nrChannels, 0);
        if(data){
            glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_X + i,
                         0, GL_RGB, width, height, 0, GL_RGB, GL_UNSIGNED_BYTE, data);
            stbi_image_free(data);
        }
        else{
            std::cout << "ERROR::FAILED_TO_LOAD_CUBEMAP_TEXTURE :" << texture_face[i] << std::endl;
            stbi_image_free(data);
        }
    }

    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);
}

void Skybox::initSkyBoxModel() {

    float vertices[] = {
            -SIZE,  SIZE, -SIZE,
            -SIZE, -SIZE, -SIZE,
            SIZE, -SIZE, -SIZE,
            SIZE, -SIZE, -SIZE,
            SIZE,  SIZE, -SIZE,
            -SIZE,  SIZE, -SIZE,

            -SIZE, -SIZE,  SIZE,
            -SIZE, -SIZE, -SIZE,
            -SIZE,  SIZE, -SIZE,
            -SIZE,  SIZE, -SIZE,
            -SIZE,  SIZE,  SIZE,
            -SIZE, -SIZE,  SIZE,

            SIZE, -SIZE, -SIZE,
            SIZE, -SIZE,  SIZE,
            SIZE,  SIZE,  SIZE,
            SIZE,  SIZE,  SIZE,
            SIZE,  SIZE, -SIZE,
            SIZE, -SIZE, -SIZE,

            -SIZE, -SIZE,  SIZE,
            -SIZE,  SIZE,  SIZE,
            SIZE,  SIZE,  SIZE,
            SIZE,  SIZE,  SIZE,
            SIZE, -SIZE,  SIZE,
            -SIZE, -SIZE,  SIZE,

            -SIZE,  SIZE, -SIZE,
            SIZE,  SIZE, -SIZE,
            SIZE,  SIZE,  SIZE,
            SIZE,  SIZE,  SIZE,
            -SIZE,  SIZE,  SIZE,
            -SIZE,  SIZE, -SIZE,

            -SIZE, -SIZE, -SIZE,
            -SIZE, -SIZE,  SIZE,
            SIZE, -SIZE, -SIZE,
            SIZE, -SIZE, -SIZE,
            -SIZE, -SIZE,  SIZE,
            SIZE, -SIZE,  SIZE
    };

    unsigned int nbVertices = 108;

    box = new Model(vertices, nbVertices);
}

void Skybox::initShader() {
    shader = new Shader("../model/skybox/vertex_skybox.glsl",
                        nullptr,
                        "../model/skybox/fragment_skybox.glsl");
}

void Skybox::initTextureBindings() {
    shader->use();
    glActiveTexture(GL_TEXTURE6);
    glBindTexture(GL_TEXTURE_CUBE_MAP, skybox_texture_id);
    shader->unuse();
}

void Skybox::initUniforms() {
    shader->use();
    shader->setMat4("projection", ProjectionMatrix);
    shader->setMat4("view", ViewMatrix);
    shader->setTexture("cubeMap", 6);
    shader->unuse();
}

void Skybox::updateUniforms(glm::mat4 projection, glm::mat4 view) {
    shader->use();
    shader->setMat4("projection", projection);
    shader->setMat4("view", view);
    shader->unuse();
}



void Skybox::render() {
    box->render(shader);
}


