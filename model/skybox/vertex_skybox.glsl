#version 440 core

layout (location = 0) in vec3 position;
out vec3 textureCoords;

uniform mat4 projection;
uniform mat4 view;

void main(){

    vec4 pos = projection * view * vec4(position, 1.0f);
    gl_Position = pos.xyzw;
    textureCoords = position;
}