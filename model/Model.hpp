//
// Created by Pierre on 12/07/2021.
//

#ifndef OPENGL_MOUNTAINS_MODEL_HPP
#define OPENGL_MOUNTAINS_MODEL_HPP

#include <GL/glew.h>
#include <GLFW/glfw3.h>


#include <vector>
#include <string>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/mat4x4.hpp>

#include "../shaders/Shader.hpp"

class Model {

public:

    Model(const float *vertices,
          unsigned nbVertices,
          const float *textcoords,
          unsigned nbTextCoords,
          const unsigned int *indices,
          unsigned nbIndices);

    Model(const float *vertices,
          unsigned nbVertices,
          const unsigned int *indices,
          unsigned nbIndices);

    Model(const float *vertices,
          unsigned nbVertices);

    virtual ~Model();

    void render(Shader *shader);

private:

    void initVAO();
    void initVAOSkybox();
    void initVAOSun();

    const float *vertices;
    unsigned nbVertices;

    const float *textCoords;
    unsigned nbTextCoords;

    const unsigned int *indices;
    unsigned nbIndices;

    GLuint VAO;
    GLuint VBO_pixel;
    GLuint VBO_tex;
    GLuint EBO;

};


#endif //OPENGL_MOUNTAINS_MODEL_HPP
