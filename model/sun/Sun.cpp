//
// Created by Pierre on 14/07/2021.
//

#include "Sun.hpp"

Sun::Sun(glm::vec3 center, glm::mat4 projection, glm::mat4 view) : center(center),
ProjectionMatrix(projection), ViewMatrix(view){
    initSunModel();
    initShader();
    initUniforms();
}

void Sun::initSunModel() {
    float sectorCount = 30.f;
    float stackCount = 30.f;

    float x, y, z, xy;                              // vertex position

    float sectorStep = 2 * PI / sectorCount;
    float stackStep = PI / stackCount;
    float sectorAngle, stackAngle;

    std::vector<float> positions;

    for(int i = 0; i <= stackCount; ++i) {

        stackAngle = PI / 2 - i * stackStep;        // starting from pi/2 to -pi/2
        xy = SUN_SIZE * cosf(stackAngle);             // r * cos(u)
        z = SUN_SIZE * sinf(stackAngle);              // r * sin(u)

        // add (sectorCount+1) vertices per stack
        // the first and last vertices have same position and normal, but different tex coords
        for (int j = 0; j <= sectorCount; ++j) {
            sectorAngle = j * sectorStep;           // starting from 0 to 2pi

            // vertex position (x, y, z)
            x = xy * cosf(sectorAngle);             // r * cos(u) * cos(v)
            y = xy * sinf(sectorAngle);             // r * cos(u) * sin(v)
            if(center.x < 500.f && center.x > -500.f)
                positions.emplace_back(x + center.x);
            else if(center.x > 0)
                positions.emplace_back(x + center.x + 15000);
            else
                positions.emplace_back(x + center.x - 15000);
            positions.emplace_back(y + center.y - 13000);
            positions.emplace_back(z + center.z + 5000);
        }
    }

    auto *vertices = new float[positions.size()];

    for(auto i = 0; i < positions.size(); i++)
        vertices[i] = positions[i];

    unsigned nrOfVertices = positions.size();

    std::vector<int> indices;
    std::vector<int> lineIndices;
    int k1, k2;
    for(int i = 0; i < stackCount; ++i)
    {
        k1 = i * (sectorCount + 1);     // beginning of current stack
        k2 = k1 + sectorCount + 1;      // beginning of next stack

        for(int j = 0; j < sectorCount; ++j, ++k1, ++k2)
        {
            // 2 triangles per sector excluding first and last stacks
            // k1 => k2 => k1+1
            if(i != 0)
            {
                indices.push_back(k1);
                indices.push_back(k2);
                indices.push_back(k1 + 1);
            }

            // k1+1 => k2 => k2+1
            if(i != (stackCount-1))
            {
                indices.push_back(k1 + 1);
                indices.push_back(k2);
                indices.push_back(k2 + 1);
            }

            // store indices for lines
            // vertical lines for all stacks, k1 => k2
            lineIndices.push_back(k1);
            lineIndices.push_back(k2);
            if(i != 0)  // horizontal lines except 1st stack, k1 => k+1
            {
                lineIndices.push_back(k1);
                lineIndices.push_back(k1 + 1);
            }
        }
    }

    GLuint *indices_array = new GLuint[indices.size() + lineIndices.size()];
    for(auto i = 0; i < indices.size(); i++)
        indices_array[i] = static_cast<GLuint>(indices[i]);
    for(auto i = 0; i < lineIndices.size(); i++)
        indices_array[i + indices.size()] = static_cast<GLuint>(lineIndices[i]);


    int nbIndices = indices.size();

    sun_model = new Model(vertices, nrOfVertices, indices_array, nbIndices);
}

void Sun::initShader() {
    shader = new Shader("../model/sun/vertex_sun.glsl",
                        nullptr,
                        "../model/sun/fragment_sun.glsl");
}

void Sun::initUniforms() {
    shader->use();
    shader->setMat4("projection", ProjectionMatrix);
    shader->setMat4("view", ViewMatrix);
    shader->unuse();
}

void Sun::updateUniforms(glm::mat4 projection, glm::mat4 view) {
    shader->use();
    shader->setMat4("projection", projection);
    shader->setMat4("view", view);
    shader->unuse();
}

void Sun::render() {
    sun_model->render(shader);
}

