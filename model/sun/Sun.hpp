//
// Created by Pierre on 14/07/2021.
//

#ifndef OPENGL_MOUNTAINS_SUN_HPP
#define OPENGL_MOUNTAINS_SUN_HPP

#include "../../shaders/Shader.hpp"
#include "../Model.hpp"
#include <vector>
#include <string>

#define SUN_SIZE 1000
#define PI 3.14

class Sun {
public:
    Sun(glm::vec3 center, glm::mat4 projection, glm::mat4 view);
    void render();

    Shader *shader;
    void updateUniforms(glm::mat4 projection, glm::mat4 view);

private:
    void initSunModel();
    void initShader();
    void initUniforms();

    Model *sun_model;
    glm::vec3 center;

    glm::mat4 ProjectionMatrix;
    glm::mat4 ViewMatrix;


};


#endif //OPENGL_MOUNTAINS_SUN_HPP
